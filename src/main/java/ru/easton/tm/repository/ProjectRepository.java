package ru.easton.tm.repository;

import ru.easton.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public Project create(final String name){
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId){
        final Project project = new Project(name, description);
        project.setUserId(userId);
        projects.add(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description){
        final Project project = findById(id);
        if(project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public void clear(final Long userId){
        List<Project> userProjects = findByUserId(userId);
        for(final Project project: userProjects)
            projects.remove(project);
    }

    public List<Project> findByUserId(final Long userId) {
        List<Project> userProjects = new ArrayList<>();
        for(final Project project: projects){
            if(project.getUserId() == null) continue;
            if(project.getUserId().equals(userId)) userProjects.add(project);
        }
        return userProjects;
    }

    public List<Project> findAll(){
        return projects;
    }

    public Project findByIndex(int index, Long userId){
        List<Project> userProjects = findByUserId(userId);
        if(index > userProjects.size() - 1) return null;
        return userProjects.get(index);
    }

    public Project findByIndex(int index){
        if(index > projects.size() - 1) return null;
        return projects.get(index);
    }

    public Project findByName(final String name, final List<Project> userProjects){
        if(name == null || name.isEmpty()) return null;
        for(final Project project: userProjects){
            if(project.getName().equals(name)) return project;
        }
        return null;
    }

    public Project findById(final Long id){
        for(final Project project: projects){
            if(project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project removeByIndex(final int index, final Long userId){
        final Project project = findByIndex(index, userId);
        if(project==null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByName(final String name, Long userId){
        final List<Project> userProjects = findByUserId(userId);
        final Project project = findByName(name, userProjects);
        if(project==null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeById(final Long id){
        final Project project = findById(id);
        if(project==null) return null;
        projects.remove(project);
        return project;
    }

}
