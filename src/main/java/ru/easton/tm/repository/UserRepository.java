package ru.easton.tm.repository;

import ru.easton.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private final List<User> users = new ArrayList<>();

    public void create(final User user) {
        users.add(user);
    }

    public void clear() {
        users.clear();
    }

    public List<User> findAll() {
        return users;
    }

    public User findByIndex(int index){
        if(index > users.size() - 1) return null;
        return users.get(index);
    }

    public User findById(final Long id){
        for(final User user: users){
            if(user.getId().equals(id)) return user;
        }
        return null;
    }

    public User findByLogin(final String login){
        for(final User user: users){
            if(user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User removeById(final Long id){
        final User user = findById(id);
        if(user==null) return null;
        users.remove(user);
        return user;
    }

    public User removeByIndex(final int index){
        final User user = findByIndex(index);
        if(user==null) return null;
        users.remove(user);
        return user;
    }

    public boolean existByLogin(String login) {
        final User user = findByLogin(login);
        return user != null;
    }
}
