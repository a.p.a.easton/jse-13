package ru.easton.tm.repository;

import ru.easton.tm.entity.Project;
import ru.easton.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId){
        final Task task = new Task(name, description);
        task.setUserId(userId);
        tasks.add(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description){
        final Task task = findById(id);
        if(task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public void clear(final Long userId) {
        List<Task> userTasks = findByUserId(userId);
        for(final Task task: userTasks)
            tasks.remove(task);
    }

    public List<Task> findByUserId(final Long userId) {
        List<Task> userTasks = new ArrayList<>();
        for(final Task task: tasks){
            if(task.getUserId() == null) continue;
            if(task.getUserId().equals(userId)) userTasks.add(task);
        }
        return userTasks;
    }

    public List<Task> findAll() {
        return tasks;
    }

    public Task findByIndex(int index, final Long userId){
        List<Task> userTasks = findByUserId(userId);
        if(index > userTasks.size() - 1) return null;
        return userTasks.get(index);
    }

    public Task findByIndex(int index){
        if(index > tasks.size() - 1) return null;
        return tasks.get(index);
    }

    public Task findByName(final String name, final List<Task> userTasks){
        if(name == null || name.isEmpty()) return null;
        for(final Task task: userTasks){
            if(task.getName().equals(name)) return task;
        }
        return null;
    }

    public Task findById(final Long id){
        for(final Task task: tasks){
            if(task.getId().equals(id)) return task;
        }
        return null;
    }

    public List<Task> findAllByProjectId(final Long projectId, final Long userId){
        final List<Task> result = new ArrayList<>();
        for(final Task task: tasks){
            final Long idProject = task.getProjectId();
            final Long idUser = task.getUserId();
            if(idProject == null) continue;
            if(idUser == null) continue;
            if(idProject.equals(projectId) && idUser.equals(userId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAllByProjectId(final Long projectId){
        final List<Task> result = new ArrayList<>();
        for(final Task task: tasks){
            final Long idProject = task.getProjectId();
            if(idProject == null) continue;
            if(idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id){
        for(final Task task: tasks){
            final Long idProject = task.getProjectId();
            if(idProject == null) continue;
            if(!idProject.equals(projectId)) continue;
            if(task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task removeByName(final String name, final Long userId){
        final List<Task> userTasks = findByUserId(userId);
        final Task task = findByName(name, userTasks);
        if(task==null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeById(final Long id){
        final Task task = findById(id);
        if(task==null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByIndex(final int index, final Long userId){
        final Task task = findByIndex(index, userId);
        if(task==null) return null;
        tasks.remove(task);
        return task;
    }

}
