package ru.easton.tm.service;

import ru.easton.tm.entity.Project;
import ru.easton.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(String name, String description, Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description, userId);
    }

    public Project update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    public void clear(final Long userId) {
        projectRepository.clear(userId);
    }

    public List<Project> findByUserId(final Long userId) {
        return projectRepository.findByUserId(userId);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findByIndex(final int index, final Long userId) {
        if(index < 0) return null;
        return projectRepository.findByIndex(index, userId);
    }

    public Project findByIndex(final int index) {
        if(index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    public Project removeByIndex(int index, final Long userId) {
        if(index < 0) return null;
        return projectRepository.removeByIndex(index, userId);
    }

    public Project removeByName(String name, Long userId) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name, userId);
    }

    public Project removeById(final Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

}
