package ru.easton.tm.service;

import ru.easton.tm.entity.Task;
import ru.easton.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public Task create(String name, String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description, userId);
    }

    public Task update(final Long id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear(final Long userId) {
        taskRepository.clear(userId);
    }

    public Task findByIndex(final int index, final Long userId) {
        if(index < 0) return null;
        return taskRepository.findByIndex(index, userId);
    }

    public Task findByIndex(final int index) {
        if(index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    public Task removeByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name, userId);
    }

    public Task removeById(final Long id) {
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    public Task removeByIndex(final int index, final Long userId) {
        if(index < 0) return null;
        return taskRepository.removeByIndex(index, userId);
    }

    public List<Task> findByUserId(Long userId) {
        return taskRepository.findByUserId(userId);
    }
}
