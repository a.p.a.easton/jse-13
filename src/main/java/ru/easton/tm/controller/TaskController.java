package ru.easton.tm.controller;

import ru.easton.tm.entity.Project;
import ru.easton.tm.entity.Task;
import ru.easton.tm.service.ProjectTaskService;
import ru.easton.tm.service.TaskService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    public int createTask(final Long userId){
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME: ");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION: ");
        final String description = scanner.nextLine();
        taskService.create(name, description, userId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByName(final Long userId){
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME: ");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name, userId);
        if(task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById(){
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("PLEASE, ENTER TASK ID: ");
        Long id = null;
        if(scanner.hasNextLong())
            id = scanner.nextLong();
        final Task task = taskService.removeById(id);
        if(task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex(final Long userId){
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX: ");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.removeByIndex(index, userId);
        if(task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex(final Long userId){
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER, TASK INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskService.findByIndex(index, userId);
        if(task == null){
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION: ");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask(final Long userId){
        if(checkAuthentication(userId)) return 0;
        System.out.println("[CLEAR TASK]");
        taskService.clear(userId);
        System.out.println("[OK]");
        return 0;
    }

    private void viewTask(final Task task){
        if(task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex(final Long userId){
        if(checkAuthentication(userId)) return 0;
        System.out.println("ENTER, TASK INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskService.findByIndex(index, userId);
        viewTask(task);
        return 0;
    }

    public int listTask(final Long userId){
        if(checkAuthentication(userId)) return 0;
        System.out.println("[LIST TASK]");
        final List<Task> tasks = taskService.findByUserId(userId);
        Collections.sort(tasks, Comparator.comparing(Task::getName));
        int index = 1;
        for(final Task task: tasks){
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void viewTasks(final List<Task> tasks){
        int index = 1;
        for(final Task task: tasks){
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    public int listTasksByProjectId(final Long userId){
        System.out.println("[LIST TASKS BY PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = projectTaskService.findAllByProjectId(projectId, userId);
        if(tasks == null || tasks.isEmpty()) {
            System.out.println("[FAIL]");
            return 0;
        }
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToProjectById(final Long userId){
        System.out.println("[ADD TASK TO PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID: ");
        final long taskId = Long.parseLong(scanner.nextLine());
        final Task task = projectTaskService.addTaskToProject(projectId, taskId, userId);
        if(task == null){
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromProjectById(final Long userId){
        System.out.println("[REMOVE TASK FROM PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID: ");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId, userId);
        return 0;
    }

}
