package ru.easton.tm;

import ru.easton.tm.controller.ProjectController;
import ru.easton.tm.controller.SystemController;
import ru.easton.tm.controller.TaskController;
import ru.easton.tm.controller.UserController;
import ru.easton.tm.entity.User;
import ru.easton.tm.enumerated.Role;
import ru.easton.tm.repository.ProjectRepository;
import ru.easton.tm.repository.TaskRepository;
import ru.easton.tm.repository.UserRepository;
import ru.easton.tm.service.ProjectService;
import ru.easton.tm.service.ProjectTaskService;
import ru.easton.tm.service.TaskService;
import ru.easton.tm.service.UserService;

import java.util.Scanner;

import static ru.easton.tm.constant.TerminalConst.*;

public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final UserService userService = new UserService(userRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService);

    private final SystemController systemController = new SystemController();

    private final UserController userController = new UserController(userService);

    private Long userId;

    {
        User test = userService.create("test", "test", Role.USER);
        User admin = userService.create("admin", "admin", Role.ADMIN);
        projectRepository.create("DEMO PROJECT 1", "DEMO PROJECT 1", test.getId());
        projectRepository.create("DEMO PROJECT 2", "DEMO PROJECT 2", admin.getId());
        projectRepository.create("DEMO PROJECT 3", "DEMO PROJECT 3", userId);
        taskRepository.create("TEST TASK 1", "TEST TASK 1", test.getId());
        taskRepository.create("TEST TASK 2", "TEST TASK 2", admin.getId());
        taskRepository.create("TEST TASK 3", "TEST TASK 3", userId);
    }

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        String command = "";
        while(!EXIT.equals(command)){
            command = scanner.nextLine();
            app.systemController.saveCommand(command);
            app.run(command);
        }
    }

    public void  run(final String[] args){
        if(args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param){
        if (param == null || param.isEmpty()) return -1;
        switch (param){
            case VERSION: return systemController.displayVersion();
            case ABOUT: return systemController.displayAbout();
            case HELP: return systemController.displayHelp();
            case COMMAND_HISTORY: return systemController.displayCommandHistory();
            case EXIT: return systemController.displayExit();

            case PROJECT_CREATE: return projectController.createProject(userId);
            case PROJECT_CLEAR: return projectController.clearProject(userId);
            case PROJECT_LIST: return projectController.listProject(userId);
            case PROJECT_VIEW: return projectController.viewProjectByIndex(userId);
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName(userId);
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex(userId);
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex(userId);

            case TASK_CREATE: return taskController.createTask(userId);
            case TASK_CLEAR: return taskController.clearTask(userId);
            case TASK_LIST: return taskController.listTask(userId);
            case TASK_VIEW: return taskController.viewTaskByIndex(userId);
            case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName(userId);
            case TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex(userId);
            case TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex(userId);
            case TASK_LIST_BY_PROJECT_ID: return taskController.listTasksByProjectId(userId);
            case TASK_ADD_TO_PROJECT_BY_ID: return taskController.addTaskToProjectById(userId);
            case TASK_REMOVE_FROM_PROJECT_BY_ID: return taskController.removeTaskFromProjectById(userId);

            case USER_CREATE: return userController.createUser();
            case USER_CLEAR: return userController.clearUser();
            case USER_LIST: return userController.listUser();
            case USER_VIEW: return userController.viewUserById();
            case USER_UPDATE: return userController.updateUserById();
            case USER_REMOVE_BY_ID: return userController.removeById();
            case USER_CHANGE_PASSWORD: return userController.changePassword(userId);

            case USER_SIGN_IN:
                userId = userController.signIn();
                return 0;
            case USER_SIGN_OUT:
                userId = userController.signOut();
                return 0;

            default: return systemController.displayError();
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public Long getUserId() {
        return userId;
    }
}
